var DB_NAME = 'omc_webpos';
var DB_VERSION = 40;
var STORE_NAME_ACTION = 'action_log';
var db;

/**
 * Connect database omc_webpos
 * @type {IDBOpenDBRequest}
 */
var request = indexedDB.open(DB_NAME, DB_VERSION);
request.onsuccess = function() {
    db = request.result;
};

/**
 * Get data of item with keypath: request_place_order_POS10-1570486286_1570486286665
 * @type {never|IDBTransaction|void}
 */
var transaction = db.transaction(STORE_NAME_ACTION, 'readwrite');
var objectStore = transaction.objectStore(STORE_NAME_ACTION);
var data = objectStore.get('request_place_order_POS10-1570486286_1570486286665');

/**
 * Update data to the item
 * @param event
 */
data.onsuccess = function(event) {
    data.result.count_request_error = 0;
    data.result.error_content = '';
    data.result.params.order.customer_id = 10120;
    objectStore.put(data.result).onsuccess = function (event){
        console.log(data.result);
    }
}