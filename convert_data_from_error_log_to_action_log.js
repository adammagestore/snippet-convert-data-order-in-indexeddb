var DB_NAME = 'omc_webpos';
var DB_VERSION = 40;
var STORE_NAME_ERR = 'error_log';
var STORE_NAME_ACTION = 'action_log';
var db;

/**
 * Connect database omc_webpos
 * @type {IDBOpenDBRequest}
 */
var request = indexedDB.open(DB_NAME, DB_VERSION);
request.onsuccess = function() {
    db = request.result;
};

getErrorOrders(db);

/**
 * Get data from error_log and save to action_log
 * @param db
 */
function getErrorOrders(db) {
    var transaction = db.transaction(STORE_NAME_ERR, 'readonly');
    var objectStore = transaction.objectStore(STORE_NAME_ERR);
    if ('getAll' in objectStore) {
        objectStore.getAll().onsuccess = function(event) {
            event.target.result.map((item) => {
                item.count_request_error = 0;
                item.error_content = '';
                saveActionOrders(db, item);
            })
        };
    }
}

/**
 * Save data to the action_log
 * @param db
 * @param item
 */
function saveActionOrders(db, item) {
    var transactionAction = db.transaction(STORE_NAME_ACTION, 'readwrite');
    var objectStoreAction = transactionAction.objectStore(STORE_NAME_ACTION);
    objectStoreAction.put(item).onsuccess = function(event) {
        console.log(item);
    }
}